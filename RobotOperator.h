/**
 * @file RobotControl.cpp
 * @Author Muhamemd Kağan Erdoğan
 * @date January 2021
 * @brief Declaration of the RobotControl
 *
 */
#pragma once
#include <string>
#include <math.h>
#include <iostream>
#include "Encryption.h"

using namespace std;
class RobotOperator {
private:
	string name;
	string surname;
	unsigned int accessCode;
	bool accessState;
	Encryption enc1;
public:
	RobotOperator(string, string, unsigned int);
	int encryptCode(int);
	int decryptCode(int);
	bool checkAccessCode(int);
	void print();
};

