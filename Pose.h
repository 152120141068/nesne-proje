/**
 * @file Pose.h
 * @Author 152120141068 Esma Deniz Uygun
 * @date January 2021
 * @brief Defining Robot's compositions
 *
 */

#pragma once
#include <math.h>
#include "Aria/ariaUtil.h"

class Pose {
private:
	float x, y, th;
public:
	Pose(float, float, float);
	Pose();
	float getX();
	float getY();
	float getTh();
	void getPose(float&, float&, float&);
	void setX(float);
	void setY(float);
	void setTh(float);
	void setPose(float, float, float);
	float findDistanceTo(Pose);
	float findAngleTo(Pose);
	bool operator==(const Pose&);
	Pose operator+(const Pose&);
	Pose operator-(const Pose&);
	Pose& operator+=(const Pose&);
	Pose& operator-=(const Pose&);
	bool operator<(const Pose&);
};