#pragma once
#include "Pose.h"
using namespace std;
class Node {
public:
	Node* next;
	Pose pose;
};
/*
 * @file Node.h
 * @Author Kubilay Doğru (kblydgru@gmail.com)
 * @date January, 2021
 * @brief Declaration of Node class
 * 
