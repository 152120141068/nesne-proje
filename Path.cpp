/**
 * @file Pose.h
 * @Author 152120141068 Esma Deniz Uygun
 * @date January 2021
 * @brief Defining Functions of Robot's movement
 *
 */


#include "Path.h"

void Path::addPos(Pose pose)
{
    Node* temp = new Node();
    temp->pose = pose;
    temp->next = NULL;
    if (head == NULL) {
        head = temp;
        head->next = tail;
    }
    else if (head != NULL && head->next == NULL) {
        tail = temp;
        tail->next = NULL;
    }
    else {
        tail->next = temp;
        tail = temp;
    }
}

void Path::print()
{
    Node* temp;
    if (head != NULL){
        temp = head;
        while (temp != NULL) {
            cout <<"Current position is:"<<temp->pose.getX()<<" : "<< temp->pose.getY()<<" : "<< temp->pose.getTh() << endl;
            temp = temp->next;
        } 
        cout << "End of path" << endl;
    }
    else {
        cout << "Path is empty" << endl;
    }
}

Pose Path::operator[](int index)
{
    return getPos(index);
}

Pose Path::getPos(int index)
{
    Node* temp;
    if (head != NULL) {
        temp = head;
        for (int i = 0; i < index; i++) {
            if (temp->next != NULL)
                temp = temp->next;
            else
                cout << "There is no data in the given index ... " << endl;
            break;
        }
        cout << "Current position is:" << temp->pose.getX() << " : " << temp->pose.getY() << " : " << temp->pose.getTh() << endl;
        return temp->pose;
    }
    else {
        cout << "Path is empty" << endl;
    }
}

bool Path::removePos(int index)
{
    Node* temp;
    if (head != NULL) {
        temp = head;
        for (int i = 0; i < index; i++) {
            if (temp->next != NULL)
                temp = temp->next;
            else {
                cout << "There is no data in the given index ... " << endl;
                return false;
            }
        }
        Node* temp1;
        temp1 = head;
        while (temp1->next != NULL) {
            if (temp1->next == temp){
                temp1->next = temp->next;
                free(temp); 
                return true;
            }
            temp1 = temp1->next;
        } 
    }
    else {
        cout << "Path is empty" << endl;
        return false;
    }
}

bool Path::insertPos(int index, Pose pose)
{ 
    Node* inserted;
    inserted->pose = pose;
    Node* temp;
    if (head != NULL) {
        temp = head;
        for (int i = 0; i < index; i++) {
            if (temp->next != NULL)
                temp = temp->next;
            else {
                cout << "There is no index in this path ..." << endl;
                return false;
            }
        }
        Node* temp1;
        temp1 = head;
        while (temp1 != NULL) {
            if (temp == head) {
                inserted->next = head;
                head = inserted;
                return true;
            }
            else if (temp1->next == temp){
                temp1->next = inserted;
                inserted->next = temp;
                return true;
            }
            else {
                 addPos(pose);
                 return true;
            }
            temp1 = temp1->next;
        }
    }
    else{
        return false;
    }
}

ostream& Path::operator<<(ostream& out)
{
    Node* temp;
    if (head != NULL) {
        temp = head;
        while (temp != NULL) {
            out << "Current position is:" << temp->pose.getX() << " : " << temp->pose.getY() << " : " << temp->pose.getTh() << endl;
            temp = temp->next;
        }
        out << "End of path" << endl;
    }
    else {
        out << "Path is empty" << endl;
    }
}

istream& Path::operator>>(istream& in)
{
    Node* temp;
    int x, y, th;
    in >> x >> y >> th;
    temp->pose.setPose(x, y, th);
    temp->next = NULL;
    if (head == NULL) {
        head = temp;
        head->next = tail;
    }
    else if (head != NULL && head->next == NULL) {
        tail = temp;
        tail->next = NULL;
    }
    else {
        tail->next = temp;
        tail = temp;
    }
}



