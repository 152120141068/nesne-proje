/**
 * @file RobotOperator.cpp
 * @Author Muhamemd Kağan Erdoğan
 * @date January 2021
 * @brief Definition of the RobotOperator
 *
 */
#include "RobotOperator.h"

RobotOperator::RobotOperator(string _name = "", string _surname = "", unsigned int _Code = 0)
{
	name = _name;
	surname = _surname;
	accessCode = encryptCode(_Code);
}

int RobotOperator::encryptCode(int Code)
{
	return enc1.encrypt(Code);
}

int RobotOperator::decryptCode(int Code)
{
	return enc1.decrypt(Code);
}

bool RobotOperator::checkAccessCode(int entryCode)
{
	if (accessCode == encryptCode(entryCode))
	{
		accessState = true;
		return accessState;
	}
	else{
		accessState = false;
		return accessState;
	}
}

void RobotOperator::print()
{
	cout << name << "\t" << surname << "Erisim durumu: "<<accessState << endl;
}
