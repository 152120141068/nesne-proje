/*
 * @file LaserSensor.h
 * @Author Osman Baki ALTIOK (osmanbakialtiok@gmail.com)
 * @date January, 2021
 * @brief Declaration of LaserSensor class
 *
 */
#pragma once
#include "PioneerRobotAPI.h"
#include <math.h>
class LaserSensor{
private:
	float* ranges[181];
	PioneerRobotAPI* robotAPI;
public:
	LaserSensor();
	float getRange(int);
	void updateSensor(float[]);
	float getMax(int&);
	float getMin(int&);
	float operator[](int);
	float getAngle(int);
	float getClosestRange(float, float, float&);
};