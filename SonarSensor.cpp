/*
 * @file SonarSensor.cpp
 * @Author Osman Baki ALTIOK (osmanbakialtiok@gmail.com)
 * @date January, 2021
 * @brief Definition of SonarSensor class
 *
 */
#include "SonarSensor.h"

float SonarSensor::getRange(int index)
{
    return *ranges[index];
}

float SonarSensor::getMax(int& index)
{
	float max = *ranges[0];
	for (int i = 0; i < 16; i++)
	{
		if (*ranges[i] > max)
			max = *ranges[i];
			index = i;
	}
	return max;
}

float SonarSensor::getMin(int& index)
{
	float min = *ranges[0];
	for (int i = 0; i < 16; i++)
	{
		if (*ranges[i] < min)
			min = *ranges[i];
			index = i;
	}
	return min;
}

void SonarSensor::updateSensor(float range[])
{
	for (int i = 0; i < 16; i++)
	{
		*ranges[i] = range[i];
	}
}

float SonarSensor::operator[](int i)
{
	return getRange(i);
}

float SonarSensor::getAngle(int index)
{
    return 0.0f;
}
