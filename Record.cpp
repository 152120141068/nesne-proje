#include "Record.h"

bool Record::openFile()
{
    file.open(fileName);
    if (file.is_open())
        return true;
    else
        return false;
}   

bool Record::closeFile()
{
    file.close();
    if (!file.is_open())
        return true;
    else
        return false;
}

void Record::setFileName(string _fileName)
{
    fileName = _fileName;
}

string Record::readLine()
{
    ifstream file(fileName);
    while(file.eof())
    getchar();
    return string();
    file.close();
}

bool Record::writeLine(string)
{
    return false;
}

fstream Record::operator<<(string)
{
    return fstream();
}

/*
 * @file Record.cpp
 * @Author Kubilay Doğru (kblydgru@gmail.com)
 * @date January, 2021
 * @brief Definition of Record class
 * 