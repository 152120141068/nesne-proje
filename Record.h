#pragma once
#include <string>
#include <fstream>
#include <iostream>
using namespace std;
class Record {
private:
	string fileName;
	fstream file;
public:
	bool openFile();
	bool closeFile();
	void setFileName(string);
	string readLine();
	bool writeLine(string);
	fstream operator<<(string);
	fstream operator>>(string);
};
/*
 * @file Record.h
 * @Author Kubilay Doğru (kblydgru@gmail.com)
 * @date January, 2021
 * @brief Decleration of Record class
 * 