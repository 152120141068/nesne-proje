/**
 * @file Pose.cpp
 * @Author 152120141068 Esma Deniz Uygun
 * @date January 2021
 * @brief Defining Functions of Robot's compositions
 *
 */


#include "Pose.h"

Pose::Pose(float _X, float _Y, float _Th) {
    x = _X;
    y = _Y;
    th = ArMath::fixAngle(_Th);
}
Pose::Pose()
{
    x = 0;
    y = 0;
    th = 0;
}
float Pose::getX()
{
    return x;
}

float Pose::getY()
{
    return y;
}

float Pose::getTh()
{
    return th;
}

void Pose::getPose(float& _X, float& _Y, float& _Th)
{
    _X = getX();
    _Y = getY();
    _Th = getTh();
}

void Pose::setX(float _X)
{
    x = _X;
}

void Pose::setY(float _Y)
{
    y = _Y;
}

void Pose::setTh(float _Th)
{

    th = ArMath::fixAngle(_Th);
   
}

void Pose::setPose(float _X, float _Y, float _Th)
{
    setX(_X);
    setY(_Y);
    setTh(_Th);
}

float Pose::findDistanceTo(Pose Pos)
{
    getPose(Pos.x, Pos.y, Pos.th);
    float sum1, sum2, sum3;
    sum1 = x - (Pos.x);
    sum1 *= sum1;
    sum2 = y - (Pos.y) - y;
    sum2 *= sum2;
    sum3 = sum1 + sum2;
    return sqrt(sum3);
}
float Pose::findAngleTo(Pose Pos)
{
    float costeta = ((x * Pos.x) + (y * Pos.y)) / ((sqrt(pow(x, 2) + pow(y, 2)) * (sqrt(pow(Pos.x, 2)) + (pow(Pos.y, 2))))); 
    return acos(costeta);
}

bool Pose::operator==(const Pose& other)
{
    if ((other.x) == x && (other.y) == y && (other.th) == th)
        return true;
    else
        return false;
}

Pose Pose::operator+(const Pose& other)
{
    Pose temp;
    temp.x = x;
    temp.y = y;
    temp.th = th;

    temp.x = x + other.x;
    temp.y = y + other.y;
    temp.th = (float)ArMath::addAngle((double)temp.th, (double)other.th);
    return temp;
}

Pose Pose::operator-(const Pose& other)
{
    Pose temp;
    temp.x = x;
    temp.y = y;
    temp.th = th;

    temp.x = x - other.x;
    temp.y = y - other.y;
    temp.th = (float)ArMath::subAngle((double)temp.th, (double)other.th);
    return temp;
}

Pose& Pose::operator+=(const Pose& other)
{
   
    setX(x + other.x);
    setY(y + other.y);
    setTh((float)ArMath::addAngle((double)th, (double)other.th));
    return *this;
}

Pose& Pose::operator-=(const Pose& other)
{
    setX(x - other.x);
    setY(y - other.y);
    setTh((float)ArMath::subAngle((double)th, (double)other.th));
    return *this;
}

bool Pose::operator<(const Pose& other)
{
    Pose origin;
    if (origin.findDistanceTo(*this) < origin.findDistanceTo(other))
        return true;
    else
        return false;   
}