/**
 * @file RobotControl.h
 * @Author Muhamemd Kağan Erdoğan
 * @date January 2021
 * @brief Declaration of the RobotControl
 *
 */
#pragma once
#include "Pose.h"
#include "PioneerRobotAPI.h"
class RobotControl {

private:
	Pose *position;
	PioneerRobotAPI *robotAPI;
	int state;
public:

	RobotControl(){}; //default
	void turnLeft();
	void turnRight();
	void forward(float);
	void backward(float);
	Pose getPose();
	void setPose(Pose);
	void stopTurn();
	void stopMove();

};