/*
 * @file SonarSensor.h
 * @Author Osman Baki ALTIOK (osmanbakialtiok@gmail.com)
 * @date January, 2021
 * @brief Declaration of SonarSensor class
 *
 */
#pragma once
#include "PioneerRobotAPI.h"
class SonarSensor {
private:
	float* ranges[16];
	PioneerRobotAPI* robotAPI;
public:

	float getRange(int);
	float getMax(int&);
	float getMin(int&);
	void updateSensor(float[]);
	float operator[](int); 
	float getAngle(int);

};