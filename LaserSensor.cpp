/*
 * @file LaserSensor.cpp
 * @Author Osman Baki ALTIOK (osmanbakialtiok@gmail.com)
 * @date January, 2021
 * @brief Definition of LaserSensor class
 * 
 */

#include "LaserSensor.h"

LaserSensor::LaserSensor()
{
    
    robotAPI->getLaserRange(*ranges);
  
}
float LaserSensor::getRange(int i)
{
    return *ranges[i];
}

void LaserSensor::updateSensor(float ranges[])
{
    robotAPI->getLaserRange(ranges);
}

float LaserSensor::getMax(int& index)
{
    int max = *ranges[0];
    for (int i = 0; i < 182; i++) {
        if (*ranges[i] > max) {

            max = *ranges[i];
            index = i;
        }
    }
    return max;
}

float LaserSensor::getMin(int& index)
{
    int min = *ranges[0];
    for (int i = 0; i < 182; i++) {
        if (*ranges[i] < min) {

            min = *ranges[i];
            index = i;
        }
    }
    return min;
}

float LaserSensor::operator[](int index)
{
    return getRange(index);
}

float LaserSensor::getAngle(int i)
{
    return *ranges[i];
}

float LaserSensor::getClosestRange(float startangle, float endangle, float& angle)
{
    int a = startangle;
    int minrange = *ranges[a];
    for (int i = a; i < endangle; i++) {
        if (*ranges[i] < minrange) {

            minrange = *ranges[i];
            angle = i;
        }
    }
    return minrange;

}
