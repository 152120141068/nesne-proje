/**
 * @file Pose.h
 * @Author 152120141068 Esma Deniz Uygun
 * @date January 2021
 * @brief Defining Robot's movement
 *
 */

#pragma once
#pragma once
#include "PioneerRobotAPI.h"
#include "Pose.h"
#include "Node.h"

using namespace std;
class Path {
private:
	Node* tail;
	Node* head;
	int number;

public:
	void addPos(Pose);
	void print();
	Pose operator[](int);
	Pose getPos(int);
	bool removePos(int);
	bool insertPos(int, Pose);
	ostream& operator<<(ostream& out);
	istream&  operator>>(istream&);

};