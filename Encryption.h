#pragma once
#include <string>
#include <math.h>
#include <iostream>
#include "RobotOperator.h"
using namespace std;

class  Encryption {

public:
	int encrypt(int Code) {
		if (Code<10000 && Code>-10000) {
			int A[3];
			int i = 0;
			while (Code > 0) {
				A[i] = Code % 10; i++;
				Code = Code / 10;
			}
			swap(A[0], A[3]);
			swap(A[1], A[2]);
			i = 0;
			while (i != 5) {
				A[i] = ((A[i] + 7) % 10);
				i++;
			}
			swap(A[0], A[2]);
			swap(A[1], A[3]);
			Code = A[0] * 1000 + A[1] * 100 + A[2] * 10 + A[3];
			return Code;
		}
		else
			cout << "Wrong Code Cannot Access to the System" << endl;
		return 0;
		
	}
	int decrypt(int Code) {
		if (Code<10000 && Code>-10000) {
			int A[3];
			int i = 0;
			while (Code > 0) {
				A[i] = Code % 10; i++;
				Code = Code / 10;
			}
			swap(A[0], A[3]);
			swap(A[1], A[2]);
			i = 0;
			while (i != 5) {
				A[i] = ((A[i] + 3) % 10);
				i++;
			}
			swap(A[2], A[0]);
			swap(A[3], A[1]);
			Code = A[0] * 1000 + A[1] * 100 + A[2] * 10 + A[3];
			return Code;
		}
		return 0;
	}
};
/*
 * @file Encyption.h
 * @Author Kubilay Doğru (kblydgru@gmail.com)
 * @date January, 2021
 * @brief Decleration of Encryption class
 * 