/**
 * @file RobotControl.cpp
 * @Author Muhamemd Kağan Erdoğan
 * @date January 2021
 * @brief Definition of the RobotControl
 *
 */
#include "RobotControl.h"


void RobotControl::turnLeft()
{
    robotAPI->turnRobot(PioneerRobotAPI::DIRECTION::left);
}

void RobotControl::turnRight()
{
    robotAPI->turnRobot(PioneerRobotAPI::DIRECTION::right);
}

void RobotControl::forward(float speed)
{
    if (speed > 0)
    robotAPI->moveRobot(speed);
    else
    robotAPI->moveRobot(-speed);
}

void RobotControl::backward(float speed)
{
    if (speed > 0)
    robotAPI->moveRobot(-speed);
    else 
    robotAPI->moveRobot(speed);
}


Pose RobotControl::getPose()
{
    return *position;
}

void RobotControl::setPose(Pose pos)
{
    position = &pos;
}

void RobotControl::stopTurn()
{
    robotAPI->stopRobot();
}

void RobotControl::stopMove()
{
    robotAPI->stopRobot();
}
